# Laptop Service
import os
import flask
from flask import request, redirect, url_for, abort
from flask_restful import Resource, Api
from pymongo import MongoClient

import logging
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.calcdb

#############
#
# API classes
#
#############

def dist(brevet):
    return float(brevet['km'])

def get_data(times, style, top):

    if times not in ["both", "close", "open"]:
        app.logger.debug("Invalid Choice")
        abort(404)
    app.logger.debug("Style: {style}")
    if style == "":
        style = "json"
    app.logger.debug({style})
    if style not in ["json", "csv"]:
        app.logger.debug("Invalid List Format")
        abort(404)

    _times = db.calcdb.find()

    if times == "open":
        brevets = [{'km' : time['km'], 'open' : time['open']} for time in _times]
    if times == 'both':
        brevets = [{'km' : time['km'], 'open' : time['open'], 'close' : time['close']} for time in _times]
    if times == 'close':
        brevets = [{'km' : time['km'], 'close' : time['close']} for time in _times]
    
    app.logger.debug({top})
    top = int(top)
    if top < 1 or top > len(brevets):
        top = len(brevets)

    brevets.sort(key = dist)

    if style == 'json':
        return {"brevets": brevets[:top]}
    
    else:
        csv = []
        for i in range(top):
            data = brevets[i]
            if times == 'both':
                csv.append(f"Brevet " + str(i+1) + f" {data['km'],data['open'],data['close']}")
            elif times == 'open':
                csv.append(f"Brevet " + str(i+1) + f" {data['km'],data['open']}")
            elif times == 'close':
                csv.append(f"Brevet " + str(i+1) + f" {data['km'],data['close']}")
        return csv

class ListAll(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('both', 'json', top = top)

class ListOpenOnly(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('open', 'json', top = top)

class ListCloseOnly(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('close', 'json', top = top)

class ListAlljson(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('both', 'json', top = top)

class ListOpenOnlyjson(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('open', 'json', top = top)

class ListCloseOnlyjson(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('close', 'json', top = top)

class ListAllCSV(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('both', 'csv', top = top)

class ListOpenCSV(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('open', 'csv', top = top)

class ListCloseCSV(Resource):
    def get(self):
        top = request.args.get("top", default = 0)
        return get_data('close', 'csv', top = top)

class ListOpenOnlytop(Resource):
    def get(self, options):
        top = request.args.get("top", default = 0, type = int)
        style = options
        return get_data('open', style, top)

class ListCloseOnlytop(Resource):
    def get(self, options):
        top = request.args.get("top", default = 0, type = int)
        style = options
        return get_data('close', style, top)

class ListAlltop(Resource):
    def get(self, options):
        top = request.args.get("top", default = 0, type = int)
        style = options
        return get_data('both', style, top)

class test(Resource):
    def get(self):
        return "testing"

api.add_resource(test, '/test')
api.add_resource(ListAll, '/listAll')
api.add_resource(ListOpenOnly, '/listOpenOnly')
api.add_resource(ListCloseOnly, '/listCloseOnly')
api.add_resource(ListAllCSV, '/listAll/csv')
api.add_resource(ListOpenCSV, '/listOpenOnly/csv')
api.add_resource(ListCloseCSV, '/listCloseOnly/csv')
api.add_resource(ListAlljson, '/listAll/json')
api.add_resource(ListOpenOnlyjson, '/listOpenOnly/json')
api.add_resource(ListCloseOnlyjson, '/listCloseOnly/json')

api.add_resource(ListOpenOnlytop, '/listOpenOnly/<options>')
api.add_resource(ListCloseOnlytop, '/listCloseOnly/<options>')
api.add_resource(ListAlltop, '/listAll/<options>')

if __name__ == "__main__":
    print("Opening for global access on port {}".format(80))
    app.run(port=80, host="0.0.0.0")

