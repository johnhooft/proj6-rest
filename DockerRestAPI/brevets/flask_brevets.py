"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging
from flask_restful import Resource, Api, reqparse
###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.calcdb
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route("/_displaytimes", methods = ['GET', 'POST'])
def _displaytimes():
    app.logger.debug("Show times")
    _times = db.calcdb.find()
    times = [time for time in _times]
    db.calcdb.remove({})
    if len(times) > 0:
        return render_template("times.html", times=times)
    else:
        return render_template('notimes.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist = request.args.get('brevet_dist', type=int)
    begin_time = request.args.get('begin_time', type=str)
    begin_date = request.args.get('begin_date', type=str)
    start_time = begin_date.format('MM-DD-YYYY') + " " + begin_time.format('HH:mm')
    app.logger.debug("start time={}".format(start_time))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist, start_time)
    close_time = acp_times.close_time(km, brevet_dist, start_time)
    app.logger.debug("open time = {}".format(open_time))
    app.logger.debug("close time = {}".format(close_time))
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/_submit', methods=['POST'])
def _submit():
    open_time = []
    close_time = []
    miles = []
    km = []
    for item in request.form.getlist("open"):
        if item != "":
            open_time.append(item)
    for item in request.form.getlist("close"):
        if item != "":
            close_time.append(item)
    for item in request.form.getlist("miles"):
        if item != "":
            miles.append(item)
    for item in request.form.getlist("km"):
        if item != "":
            km.append(item)
    if len(open_time) == 0:
        return render_template('empty.html')
    for i in range(len(open_time)):
        times_doc = {
            'open': open_time[i],
            'close': close_time[i],
            'miles': miles[i],
            'km': km[i],
        }
        db.calcdb.insert_one(times_doc)

    return render_template('calc.html')

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
