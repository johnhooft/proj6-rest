<html>
    <head>
        <title>Consumer Program</title>
    </head>

    <body>
        <h1>Consumer program to expose API's</h1>
        <h2>List all open and close times</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/json');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
		$km = $b->km;
                $open = $b->open;
                $close = $b->close;
		echo "<li> Distance: $km km. Open time: $open Close time: $close</li>";
            }
            ?>
	</ul>
	<h2>List all open times</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
                $km = $b->km;
                $open = $b->open;
                echo "<li> Distance: $km km. Open time: $open</li>";
            }
            ?>
        </ul>
	</ul>
        <h2>List all close times</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
                $km = $b->km;
                $close = $b->close;
                echo "<li> Distance: $km km. Close time: $close</li>";
            }
            ?>
        </ul>
        <h2>List of all first open and close times</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/json?top=1');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
                $km = $b->km;
                $open = $b->open;
                $close = $b->close;
                echo "<li>Distance: $km km. Open time: $open Close time: $close</li>";
            }
            ?>
        </ul>
        </ul>
        <h2>List of first three close times</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json?top=3');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
                $km = $b->km;
                $close = $b->close;
                echo "<li>Distance: $km km. Close time: $close</li>";
            }
            ?>
	</ul>
	<h2>List of first two open time</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=2');
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
                $km = $b->km;
                $open = $b->open;
                echo "<li> Distance: $km km. Open time: $open</li>";
            }
            ?>
        </ul>

    </body>
</html>

